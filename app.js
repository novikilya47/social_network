import express from 'express';
import authRouter from './routes/authRouter.js';
import userRouter from './routes/userRouter.js';
import postRouter from './routes/postRouter.js';
import searchRouter from './routes/searchRouter.js';
import chatRouter from './routes/chatRouter.js';
import {sendMessageToChat} from './controllers/chatController.js'
import {delMessagefromChat} from './controllers/chatController.js'
import mongoose from 'mongoose';
import cors from 'cors';
import {Server} from "socket.io"


const app = express();

app.use(cors());
app.use(express.urlencoded({extended: true}));
app.use(express.json())
app.use("/auth", authRouter);
app.use("/user", userRouter);
app.use("/post", postRouter);
app.use("/search", searchRouter);
app.use("/chats", chatRouter)

mongoose.connect("mongodb://localhost:27017/", { useUnifiedTopology: true, useNewUrlParser: true }, function (err) {
    if (err) { return console.log('Something broke!'); }
    
});

const server = app.listen(4000, function () {
        console.log("Сервер работает");
    });

const io = new Server(server)
io.on("connection", (socket) => {
    socket.on('sendMessage', async (message) => {
        const messages = await sendMessageToChat(message)
        io.emit('tweet', messages);
    })
    socket.on('delMessage', async (delmessage) => {
        const messages = await delMessagefromChat(delmessage)
        io.emit('tweet', messages);
    })
})
