import jwt from 'jsonwebtoken';
const accessTokenSecret = '12345';
import { User } from "../models/userModel.js";

export const registration = async function (req, res) {
    try{
        if (!/\S+@\S+\.\S+/.test(req.body.email) || !req.body.email.trim()) {
            res.status(400).json(`Неверный формат почты`);
        } else {
            const user = await User.findOne({ email: req.body.email });
            if (user) {
                res.status(500).json(`Данный пользователь уже зарегестрирован`);
            } else {
                User.create({email: req.body.email, password: req.body.password, name: req.body.name, surname: req.body.surname}, function (err, doc) {
                    if (err) { return res.status(400).send(err); }
                    res.status(201).json(`Новый пользователь зарегистрирован`);
                });
            }
        }
    } catch(e){
        return res.status(500);
    }
};

export const login = async function (req, res) {
    try{
        const user = await User.findOne({email: req.body.email, password: req.body.password});
        if (user) {
            const accessToken = jwt.sign({email: user.email, _id: user._id, name: user.name }, accessTokenSecret);
            res.status(200).json({token : `Bearer ${accessToken}`, user : user})
        } else {
            res.sendStatus(400);
        }
    }catch(e){
        return res.status(500);
    }
};

export const authMiddleware = function (req, res, next) {
    try{
        const token = req.headers.authorization.split(' ')[1];
        if (token) {
            const decoded = jwt.verify(token, accessTokenSecret);
            req.user = decoded;
            next();
        } else {
            res.status(400).send("Не проведена авторизация");
        }
    }catch(e){
        return res.status(500);
    }
};

export const auth = async function (req, res) {
        try {
            const user = await User.findOne({_id: req.user._id})
            return res.status(200).json({user})
        } catch (e) {
            return res.status(500);
        }
}

export const friend = async function (req, res) {
    try {
        const targetUser = await User.findOne({_id: req.body._id})
        return res.status(200).json({targetUser})
    } catch (e) {
        return res.status(500);
    }
}
