import { User } from "../models/userModel.js";
import { Chat} from "../models/chatModel.js";
import { Message } from "../models/messageModel.js";

export const getChat = async (req, res) => {
    try {
        const chat = await Chat.findById(req.body._id)
        const user = await User.findById(req.body.userId)
        res.json({chat, user})
    } catch (e) {
        res.status(400).send(e)
    }
};

export const sendMessageToChat = async (mes) => {
    try {
        const message = await Message.create({author: mes.myId, text: mes.mes });
        const chat = await Chat.findById(mes.chatId)
        chat.messages.push(message._id)
        await chat.save()
        const user = await User.findById(mes.myId)
        return({chat, user})
    } catch (e) {
        console.log(e)
    }
};

export const delMessagefromChat = async (del) => {
    try {
        let chat = await Chat.findById(del.chatId);
        for (let i = 0; i < chat.messages.length; i++) {
            if (chat.messages[i]._id.toString() === del.mesId.toString()) {
                chat.messages.splice(i, 1);
                await chat.save();
            }
        }
        await Message.findByIdAndDelete(del.mesId);
        const user = await User.findById(del.myId)
        return({chat, user})        
    } catch (e) {
        console.log(e)
    }
};

