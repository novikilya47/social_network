import {Chat} from "../models/chatModel.js";
import {User} from "../models/userModel.js";

export const updateAvatar = async function (req, res) {
    try {
        if (req.body.avatar.trim().length > 1){
            await User.updateOne({_id: req.body._id}, {avatar: req.body.avatar});
            let user = await User.findOne({ _id: req.body._id});
            return res.status(201).json({user});
        } else {
            return res.status(400).json(`Пустое значение`);
        }
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};

export const addFriend = async (req, res) => {
    try {
        const user = await User.findOne({_id: req.body.firstUserId})
        const user2 = await User.findOne({_id: req.body.secondUserId});
        user.friends.push(req.body.secondUserId);
        await user.save();
        for (let i = 0; i < user.invitations.length; i++) {
            if (user.invitations[i]._id.toString() === user2._id.toString()) {
                user.invitations.splice(i, 1);
                await user.save();
            }
        }
        user2.friends.push(user._id);
        await user2.save();
        for (let i = 0; i < user2.requests.length; i++) {
            if (user2.requests[i]._id.toString() === user._id.toString()) {
                user2.requests.splice(i, 1);
                await user2.save();
            }
        }
        let chat = await Chat.findOne({users: { $all: [req.body.firstUserId, req.body.secondUserId]}})
        if (chat) {
            return res.status(201).json({user});
        } else {
            await Chat.create({users:[req.body.firstUserId, req.body.secondUserId], messages:[]});
            chat = await Chat.findOne({users: { $all: [req.body.firstUserId, req.body.secondUserId]}})
            user.chats.push(chat._id)
            await user.save();
            user2.chats.push(chat._id)
            await user2.save();
            return res.status(201).json({user});  
        } 
    } catch (e) {
        return res.status(500).send(`Неверные параметры запроса ${e}`);
    }
};


export const deleteFriend = async (req, res) => {
    try {
        let user = await User.findOne({_id: req.body.firstUserId}),
            user2 = await User.findOne({_id: req.body.secondUserId});

        if (user) {
            for (let i = 0; i < user.friends.length; i++) {
                if (user.friends[i]._id.toString() === user2._id.toString()) {
                    user.friends.splice(i, 1);
                    await user.save();
                }
            }

            for (let i = 0; i < user2.friends.length; i++) {
                if (user2.friends[i]._id.toString() === user._id.toString()) {
                    user2.friends.splice(i, 1);
                    await user2.save();
                }
            }
            return res.status(201).json({user});
        } else {
            res.status(400).send('Не удалось найти данный пост');
        }
    } catch (e) {
        res.status(400).send(`Неверные параметры запроса. ${e}`);
    }
};

export const deleteUser = async (req, res) => {
    try {
        const user = await User.findOne({_id: req.body.user})
        console.log(user)
    } catch (e) {
        return res.status(500).send(`Неверные параметры запроса ${e}`);
    }
};

export const addFriendRequest = async (req, res) => {
    try {
        const user = await User.findOne({_id: req.body.firstUserId})
        const user2 = await User.findOne({_id: req.body.secondUserId});
        user2.invitations.push(user._id);
        await user2.save();
        user.requests.push(user2._id)
        await user.save();
        return res.status(201).json({user});   
    } catch (e) {
        return res.status(500).send(`Неверные параметры запроса ${e}`);
    }
};

export const deleteFriendRequest = async (req, res) => {
    try {
        const user = await User.findOne({_id: req.body.firstUserId})
        const user2 = await User.findOne({_id: req.body.secondUserId});
        for (let i = 0; i < user.requests.length; i++) {
            if (user.requests[i]._id.toString() === user2._id.toString()) {
                user.requests.splice(i, 1);
                await user.save();
            }
        }
        for (let i = 0; i < user2.invitations.length; i++) {
            if (user2.invitations[i]._id.toString() === user._id.toString()) {
                user2.invitations.splice(i, 1);
                await user2.save();
            }
        }
        return res.status(201).json({user});   
    } catch (e) {
        return res.status(500).send(`Неверные параметры запроса ${e}`);
    }
};


export const refuseFriend = async (req, res) => {
    try {
        const user = await User.findOne({_id: req.body.firstUserId})
        const user2 = await User.findOne({_id: req.body.secondUserId});
        for (let i = 0; i < user.invitations.length; i++) {
            if (user.invitations[i]._id.toString() === user2._id.toString()) {
                user.invitations.splice(i, 1);
                await user.save();
            }
        }
        for (let i = 0; i < user2.requests.length; i++) {
            if (user2.requests[i]._id.toString() === user._id.toString()) {
                user2.requests.splice(i, 1);
                await user2.save();
            }
        }
        return res.status(201).json({user});   
    } catch (e) {
        return res.status(500).send(`Неверные параметры запроса ${e}`);
    }
};





