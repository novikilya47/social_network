import { User } from "../models/userModel.js";

export const getAllUsers = async function (req, res) {
    try {
        const usersList = await User.find({});
        return res.status(201).json({usersList});
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};
