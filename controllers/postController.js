import { Post } from '../models/postModel.js';
import { User } from '../models/userModel.js';
import { Comment } from '../models/commentModel.js';

export const createPost = async (req, res) => {
    try {
        const user = await User.findOne({_id: req.body._id})
        const newPost = await Post.create({text : req.body.text, img : req.body.img, date : req.body.date});
        newPost.user = req.body._id;
        await newPost.save();
        user.posts.push(newPost._id);
        await user.save();
        return res.status(201).json({user});   
    } catch (e) {
        return res.status(500).send(`Неверные параметры запроса ${e}`);
    }
};

export const deletePost = async (req, res) => {
    try {
        let post = await Post.findOne({ _id: req.body.postId}),
            user = await User.findOne({ _id: req.body.userId});

        if (post) {
            await post.comments.forEach(element => {
                Comment.deleteOne({_id : element._id}, function (err, result) {
                    if (err) return console.log(err);
                });
            });

            await Post.deleteOne({ _id: req.body.postId}, function (err, result) {
                if (err) return console.log(err);
            });

            for (let i = 0; i < user.posts.length; i++) {
                if (user.posts[i]._id.toString() === post._id.toString()) {
                    user.posts.splice(i, 1);
                    await user.save();
                }
            }
            return res.status(201).json({user});
        } else {
            res.status(400).send('Не удалось найти данный пост');
        }
    } catch (e) {
        res.status(400).send(`Неверные параметры запроса. ${e}`);
    }
};

export const createPostComment = async (req, res) => {
    try {
        let post = await Post.findOne({_id: req.body.post_id});
        let newComment = await Comment.create({text : req.body.text, date : req.body.date, user: req.body.user_id});
        post.comments.push(newComment._id)
        await post.save();
        let user = await User.findOne({_id: req.body.user_id});
        let targetUser = await User.findOne({_id: req.body.targetUser_id});
        if (targetUser) { return res.status(201).json({user, targetUser});
        } else{
            return res.status(201).json({user});
        }  
    } catch (e) {
        return res.status(500).send(`Неверные параметры запроса ${e}`);
    }
};

export const deletePostComment = async (req, res) => {
    try {
        let post = await Post.findOne({_id: req.body.postId});
        let comment = await Comment.findOne({_id: req.body.commentId});
        
        Comment.deleteOne({ _id: req.body.commentId}, function (err, result) {
            if (err) return console.log(err);
        });

        for (let i = 0; i < post.comments.length; i++) {
            if (post.comments[i]._id.toString() === comment._id.toString()) {
                post.comments.splice(i, 1);
                await post.save();
            }
        }
        let user = await User.findOne({ _id: req.body.userId});
        let targetUser = await User.findOne({_id: req.body.targetUserId});
        if (targetUser) { return res.status(201).json({user, targetUser});
        } else{
            return res.status(201).json({user});
        }
    } catch (e) {
        return res.status(500).send(`Неверные параметры запроса ${e}`);
    }
};

export const getLikes = async (req, res) => {
    try {
        let post = await Post.findOne({_id: req.body.postId});
        let user = await User.findOne({_id: req.body.userId});
        let targetUser = await User.findOne({_id: req.body.targetUserId});
        if (post.likes.length === 0) {
            post.likes.push(req.body.userId)
            await post.save();
            user = await User.findOne({_id: req.body.userId});
            targetUser = await User.findOne({_id: req.body.targetUserId});
            if (targetUser) {
                return res.status(201).json({user, targetUser});
            } else{
                return res.status(201).json({user});
            }
        } else {
            for (let i = 0; i < post.likes.length; i++) {
                if (post.likes[i].toString() === user._id.toString()) {
                    post.likes.splice(i, 1);
                    await post.save();
                    user = await User.findOne({_id: req.body.userId});
                    targetUser = await User.findOne({_id: req.body.targetUserId});
                    if (targetUser) {
                        return res.status(201).json({user, targetUser});
                    } else{
                        return res.status(201).json({user});
                    }
                } 
            }
            for (let i = 0; i < post.likes.length; i++) {
                if (post.likes[i].toString() !== user._id.toString()){
                    post.likes.push(req.body.userId)
                    await post.save();
                    user = await User.findOne({_id: req.body.userId});
                    targetUser = await User.findOne({_id: req.body.targetUserId});
                    if (targetUser) {
                        return res.status(201).json({user, targetUser});
                    } else{
                        return res.status(201).json({user});
                    }
                }
            }
        }
    } catch (e){
        return res.status(500).send(`Неверные параметры запроса ${e}`);
    }
};
