import express from 'express';
const chatRouter = express.Router();
import {getChat} from "../controllers/chatController.js";

chatRouter.put("/getchat", getChat);

export default chatRouter;