import express from 'express';
const authRouter = express.Router();
import {login, registration, authMiddleware, auth, friend} from "../controllers/authController.js";

authRouter.post('/registration', registration);
authRouter.post('/login', login);
authRouter.get('/auth', authMiddleware, auth);
authRouter.post('/friend', authMiddleware, friend);

export default authRouter;
