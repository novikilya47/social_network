import express from 'express';
const searchRouter = express.Router();
import {getAllUsers} from "../controllers/friendsController.js";
import {authMiddleware} from "../controllers/authController.js";

searchRouter.get('/getUsers',authMiddleware, getAllUsers);

export default searchRouter;