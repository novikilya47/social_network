import express from 'express';
const postRouter = express.Router();
import {createPost, deletePost, createPostComment, deletePostComment, getLikes} from "../controllers/postController.js";
import {authMiddleware} from "../controllers/authController.js";

postRouter.post('/createpost', authMiddleware, createPost);
postRouter.delete('/deletepost', authMiddleware, deletePost);
postRouter.post('/createpostcomment', authMiddleware, createPostComment);
postRouter.delete('/deletepostcomment', authMiddleware, deletePostComment);
postRouter.put('/like', authMiddleware, getLikes);

export default postRouter;