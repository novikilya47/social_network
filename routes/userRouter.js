import express from 'express';
const userRouter = express.Router();
import {updateAvatar, addFriend, deleteFriend, deleteUser, addFriendRequest, deleteFriendRequest, refuseFriend} from "../controllers/userController.js";
import {authMiddleware} from "../controllers/authController.js";

userRouter.post('/updateavatar', authMiddleware, updateAvatar);
userRouter.post('/addfriend', authMiddleware, addFriend);
userRouter.delete('/deletefriend', authMiddleware, deleteFriend);
userRouter.delete('/deleteuser', authMiddleware, deleteUser);
userRouter.post('/addfriendrequest', authMiddleware, addFriendRequest);
userRouter.delete('/deletefriendrequest', authMiddleware, deleteFriendRequest);
userRouter.delete('/refusefriend', authMiddleware, refuseFriend);

export default userRouter;