import {sendNewAvatar, sendNewPost, deleteOnePost, sendFriends, sendDeleteFriends, sendNewPostComment, deleteOneComment, sendLike, sendDeleteUser, sendRequestFriend, sendDeleteRequestFriend, sendRefuseFriend} from "../services/user.service";
import {getNewAvatar, getNewPost, getResultOfDelete, getResultOfFriends, getResultOfDeleteFriends, getNewPostComment, getResultOfDeleteComment, updateLike, updateLikeFriends, getResultOfDeleteUser, getNewPostCommentFriends, getResultOfDeleteCommentFriends, getResultOfRequestFriend, getResultOfDeleteRequestFriend, getResultOfRefuseFriend} from "../actionCreators/action";

export const avatar = (data : any) => (dispatch : any) => {sendNewAvatar(data).then((data) => {dispatch(getNewAvatar(data))})};
export const post = (data : any) => (dispatch : any) => {sendNewPost(data).then((data) => {dispatch(getNewPost(data))})};
export const deletePost = (data : any) => (dispatch : any) => {deleteOnePost(data).then((data) => {dispatch(getResultOfDelete(data))})};
export const addFriend = (data : any) => (dispatch : any) => {sendFriends(data).then((data) => {dispatch(getResultOfFriends(data))})};
export const deleteFriend = (data : any) => (dispatch : any) => {sendDeleteFriends(data).then((data) => {dispatch(getResultOfDeleteFriends(data))})};
export const postComment = (data : any) => (dispatch : any) => {sendNewPostComment(data).then((data) => {dispatch(getNewPostComment(data)); dispatch(getNewPostCommentFriends(data))})};
export const deleteComment = (data : any) => (dispatch : any) => {deleteOneComment(data).then((data : any) => {dispatch(getResultOfDeleteComment(data)); dispatch(getResultOfDeleteCommentFriends(data))})};
export const likePost = (data : any) => (dispatch : any) => {sendLike(data).then((data : any) => {dispatch(updateLike(data)); dispatch(updateLikeFriends(data))})};
export const deleteUser = (data : any) => (dispatch : any) => {sendDeleteUser(data).then((data : any) => {dispatch(getResultOfDeleteUser(data))})};
export const requestFriend = (data : any) => (dispatch : any) => {sendRequestFriend(data).then((data : any) => {dispatch(getResultOfRequestFriend(data))})};
export const deleteRequestFriend = (data : any) => (dispatch : any) => {sendDeleteRequestFriend(data).then((data : any) => {dispatch(getResultOfDeleteRequestFriend(data))})};
export const refuseFriend = (data : any) => (dispatch : any) => {sendRefuseFriend(data).then((data : any) => {dispatch(getResultOfRefuseFriend(data))})};


