import { registration, authorization, find, findFriend, findChat} from "../services/auth.service";
import { registrationUser, authorizationUser, getCurrentUser, getCurrentFriend, getCurrentChat} from "../actionCreators/action";
import { LOGIN_USER_LOAD, LOGIN_USER_ERROR, GET_USER_LOAD, GET_USER_ERROR} from "../Constants/constants";

export const registrationNewUser = (data : any) => (dispatch : any) => {registration(data).then((data) => {dispatch(registrationUser(data))})};

export const loginUser = (data : any) => (dispatch : any) => {
    dispatch({type: LOGIN_USER_LOAD})
    authorization(data)
    .then(
        (data) => {
            dispatch(authorizationUser(data)); 
            localStorage.setItem("token", data.token)
        }
    )
    .catch(() => {dispatch({type: LOGIN_USER_ERROR})});
};

export const getUser = () => (dispatch : any) => {
    dispatch({type: GET_USER_LOAD})
    find()
    .then((data) => {dispatch(getCurrentUser(data))})
    .catch(() => {dispatch({type: GET_USER_ERROR})});
}


export const getFriend = (data : any) => (dispatch : any) => {
    findFriend(data)
    .then((data:any) => {dispatch(getCurrentFriend(data))})
}

export const getChat = (data : any) => (dispatch : any) => {
    findChat(data)
    .then((data:any) => {dispatch(getCurrentChat(data))})
}




    