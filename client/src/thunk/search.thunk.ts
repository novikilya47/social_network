import {getUsersList} from "../actionCreators/action";
import { findAll } from "../services/search.service";

export const getAllUsers = () => (dispatch : any) => {
    findAll()
    .then((data) => {dispatch(getUsersList(data))})
    }
