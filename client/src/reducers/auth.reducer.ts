import {REG_USER, LOGIN_USER_LOAD, LOGIN_USER, LOGIN_USER_ERROR, LOGOUT_USER, GET_USER, DELETE_POST_COMMENT, ADD_REQUEST_FRIEND,
GET_USER_ERROR, GET_USER_LOAD, AVATAR, SEND_POST, DELETE_POST, SEND_FRIEND, DELETE_FRIEND, SEND_POST_COMMENT, GET_LIKES, DELETE_USER, REQUEST_FRIEND, REFUSE_FRIEND} from "../Constants/constants";

const defaultState = {
    isAuth : false,
    token : "",
    currentUser : {
        posts: [],
        friends: [],
        requests : [],
        invitations : [],
        chats : [],
        name : ''
    },
    loading : false,
}

export default function reducer (state = defaultState, action : any){
    switch (action.type) {
        case REG_USER:
            return {
                ...state,
                regMessage: action.data,
            };

        case LOGIN_USER_LOAD:
            return {
                ...state,
                loading : true
            }
 
        case LOGIN_USER:
            return {
                ...state,
                token: action.data.token,
                currentUser : action.data.user,
                message : action.data.message,
                loading: false,
                isAuth : true
            }; 
            
        case LOGIN_USER_ERROR:
            return {
                ...state,
                loading: false,
                message : "Неверный логин или пароль"
            }; 

        case LOGOUT_USER:
            localStorage.removeItem("token")
            return {
                ...state,
                currentUser : {},
                isAuth : false,
            };

        case GET_USER_LOAD:
            return {
                ...state,
                loading : true
            }

        case GET_USER:
            console.log(action.data.user)
            return {
                currentUser : action.data.user,
                loading : false,
                isAuth : true
            };

        case GET_USER_ERROR:
            return {
                ...state,
                loading: false,
                message : "Неверный логин или пароль"
            }; 

        case AVATAR:
            return {
                ...state,
                currentUser : action.data.user,     
            };

        case SEND_POST:
            return{
                ...state,
                currentUser : action.data.user,
            }

        case DELETE_POST:
            return{
                ...state,
                currentUser : action.data.user,
            }

        case SEND_FRIEND:
            return{
                ...state,
                currentUser : action.data.user,
            }

        case REQUEST_FRIEND:
            return{
                ...state,
                currentUser : action.data.user,
            }

        case ADD_REQUEST_FRIEND:
            return{
                ...state,
                currentUser : action.data.user,
            }

        case REFUSE_FRIEND:
            return{
                ...state,
                currentUser : action.data.user,
            }

        case DELETE_FRIEND:
            return{
                ...state,
                currentUser : action.data.user,
            }

        case SEND_POST_COMMENT:
            return{
                ...state,
                currentUser : action.data.user,
            }

        case DELETE_POST_COMMENT:
            return{
                ...state,
                currentUser : action.data.user,
            }

        case GET_LIKES:
            return{
                ...state,
                currentUser : action.data.user,
            }  

        case DELETE_USER:
            return{
                ...state,
                currentUser : action.data.user,
            }   
            
        default:
            return state;
    }
};
