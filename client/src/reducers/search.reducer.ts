import {GET_ALL_USERS, GET_FRIEND, GET_LIKES_FRIENDS, SEND_POST_COMMENT_FRIENDS, DELETE_POST_COMMENT_FRIENDS} from "../Constants/constants";
    
    const defaultState = { 
        usersList : [],
        targetUser : {
            posts : [],
            _id : ''
        },
    }
    
    export default function reducer (state = defaultState, action : any){
        switch (action.type) {
            case GET_ALL_USERS:
                return{     
                    ...state,
                    usersList : action.data.usersList
                }
            
            case GET_FRIEND:
                return{
                    ...state,
                    targetUser : action.data.targetUser
                }

            case GET_LIKES_FRIENDS:
                return{
                    ...state,
                    targetUser : action.data.targetUser
                }

            case SEND_POST_COMMENT_FRIENDS:
                return{
                    ...state,
                    targetUser : action.data.targetUser
                }

            case DELETE_POST_COMMENT_FRIENDS:
                return{
                    ...state,
                    targetUser : action.data.targetUser
                }

            default:
                return state;
        }
    };