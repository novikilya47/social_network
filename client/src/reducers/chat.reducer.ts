import {GET_CHAT, GET_CHAT_SOCKET} from "../Constants/constants";
    
    const defaultState = { 
        targetChat : {
            messages: [],
            users : [], 
        },
        friend : ''
    }
      
    export default function reducer (state = defaultState, action : any){
        switch (action.type) {
            case GET_CHAT:
                return{
                    ...state,
                    targetChat : action.data.chat,
                    friend : action.data.chat.users.filter((user:any)=>(user._id !== action.data.user._id))[0]
                }

            case GET_CHAT_SOCKET:
                return{
                    ...state,
                    targetChat : action.data.chat,
                    friend : action.data.chat.users.filter((user:any)=>(user._id !== action.data.user._id))[0]
                }
            default:
                return state;
        }
    };