import {REG_USER, LOGIN_USER, LOGOUT_USER, GET_USER, AVATAR, SEND_POST_COMMENT, GET_LIKES, DELETE_USER, GET_LIKES_FRIENDS,
SEND_POST, DELETE_POST, GET_ALL_USERS, SEND_FRIEND, DELETE_FRIEND, GET_FRIEND, REQUEST_FRIEND, ADD_REQUEST_FRIEND,
DELETE_POST_COMMENT, SEND_POST_COMMENT_FRIENDS, DELETE_POST_COMMENT_FRIENDS, REFUSE_FRIEND, GET_CHAT, GET_CHAT_SOCKET} from "../Constants/constants";
export const registrationUser = (data: any) => ({type: REG_USER, data});
export const authorizationUser = (data: any) => ({type: LOGIN_USER, data});
export const exitUser = () => ({type: LOGOUT_USER});
export const getCurrentUser = (data: any) => ({type: GET_USER, data});
export const getNewAvatar = (data:any) => ({type: AVATAR, data});
export const getNewPost = (data:any) => ({type: SEND_POST, data});
export const getResultOfDelete = (data:any) => ({type: DELETE_POST, data});
export const getUsersList = (data:any) => ({type: GET_ALL_USERS, data});
export const getResultOfFriends = (data:any) => ({type: SEND_FRIEND, data});
export const getResultOfDeleteFriends = (data:any) => ({type: DELETE_FRIEND, data});
export const getCurrentFriend = (data:any) => ({type: GET_FRIEND, data});
export const getNewPostComment = (data:any) => ({type: SEND_POST_COMMENT, data});
export const getNewPostCommentFriends = (data:any) => ({type: SEND_POST_COMMENT_FRIENDS, data});
export const getResultOfDeleteComment = (data:any) => ({type: DELETE_POST_COMMENT, data});
export const getResultOfDeleteCommentFriends = (data:any) => ({type: DELETE_POST_COMMENT_FRIENDS, data});
export const updateLike = (data:any) => ({type: GET_LIKES, data});
export const updateLikeFriends = (data:any) => ({type: GET_LIKES_FRIENDS, data});
export const getResultOfDeleteUser = (data:any) => ({type: DELETE_USER, data});
export const getResultOfRequestFriend = (data:any) => ({type: REQUEST_FRIEND, data});
export const getResultOfDeleteRequestFriend = (data:any) => ({type: ADD_REQUEST_FRIEND, data});
export const getResultOfRefuseFriend = (data:any) => ({type: REFUSE_FRIEND, data});
export const getCurrentChat = (data:any) => ({type: GET_CHAT, data});
export const getCurrentChatSocket = (data:any) => ({type: GET_CHAT_SOCKET, data});











