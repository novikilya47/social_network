import RegistUser from "./components/AuthAndRegist/RegistationForm";
import AuthUser from "./components/AuthAndRegist/AuthForm";
import UserPage from "./components/UserPage/UserPage";
import Footer from "./components/Footer/Footer"
import Messages from "./components/Messages/Messages"
import Chat from "./components/Messages/Chat"
import FindFriends from "./components/FindFriends";
import TargetFriend from "./components/TargetFriend";
import Lenta from "./components/Lenta/Lenta";
import Friends from "./components/Friends/Friends";
import Requests from "./components/Requests/Requests";
import {BrowserRouter} from 'react-router-dom';
import {Route} from "react-router-dom";
import Navbar from "./components/Navbar/Navbar";
import "./styles.css"
import {connect} from "react-redux";

function App (props : any) {

    return (
            <BrowserRouter>
                <Navbar />           
                        <Route exact path='/' component={AuthUser}/> 
                        <Route exact path='/regist' component={RegistUser}/>
                        <Route exact path='/profile' component={UserPage}/>   
                        <Route exact path='/profile/findFriends' component={FindFriends}/>
                        <Route path='/profile/targetFriend' component={TargetFriend}/>
                        <Route exact path='/profile/friends' component={Friends}/>     
                        <Route exact path='/profile/lenta' component={Lenta}/>
                        <Route exact path='/profile/messages' component={Messages}/> 
                        <Route path='/profile/chat' component={Chat}/>
                        <Route exact path='/profile/requests' component={Requests}/> 
                        {props.loading ? <div className="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div> : <Footer /> }              
            </BrowserRouter>
    );
    
}

const mapStateToProps = (state : any) => ({
    loading : state.auth.loading
});

export default connect(
    mapStateToProps,
)(App);
