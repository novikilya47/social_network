export async function registration(data : any) {
    return await fetch(`http://localhost:4000/auth/registration`, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    }).then(res => res.json())
};

export async function authorization(data : any) {
    return await fetch(`http://localhost:4000/auth/login`, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
    .then(res => res.json());    
};

export async function find() {
    return await fetch(`http://localhost:4000/auth/auth`, {
        method: 'GET',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : `${localStorage.getItem('token')}`
        },
    }).then(res => res.json())
};

export async function findFriend(data:any) {
    return await fetch(`http://localhost:4000/auth/friend`, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : `${localStorage.getItem('token')}`
        },
    }).then(res => res.json())
};

export async function findChat(data:any) {
    return await fetch(`http://localhost:4000/chats/getchat`, {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : `${localStorage.getItem('token')}`
        },
    }).then(res => res.json())
};



