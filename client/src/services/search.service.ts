export async function findAll() {
    return await fetch(`http://localhost:4000/search/getUsers`, {
        method: 'GET',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : `${localStorage.getItem('token')}`
        },
    }).then(res => res.json())
};