export async function sendNewAvatar (data : any) {
    return await fetch(`http://localhost:4000/user/updateavatar`, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : `${localStorage.getItem('token')}`
        },
    }).then(res => res.json())
};

export async function sendNewPost (data : any) {
    return await fetch(`http://localhost:4000/post/createpost`, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : `${localStorage.getItem('token')}`
        },
    }).then(res => res.json())
};

export async function deleteOnePost (data : any) {
    return await fetch(`http://localhost:4000/post/deletepost`, {
        method: 'DELETE',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : `${localStorage.getItem('token')}`
        },
    }).then(res => res.json())
};

export async function sendFriends(data : any) {
    return await fetch(`http://localhost:4000/user/addfriend`, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : `${localStorage.getItem('token')}`
        },
    }).then(res => res.json())
};

export async function sendDeleteFriends(data : any) {
    return await fetch(`http://localhost:4000/user/deletefriend`, {
        method: 'DELETE',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : `${localStorage.getItem('token')}`
        },
    }).then(res => res.json())
};

export async function sendNewPostComment (data : any) {
    return await fetch(`http://localhost:4000/post/createpostcomment`, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : `${localStorage.getItem('token')}`
        },
    }).then(res => res.json())
};

export async function deleteOneComment (data : any) {
    return await fetch(`http://localhost:4000/post/deletepostcomment`, {
        method: 'DELETE',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : `${localStorage.getItem('token')}`
        },
    }).then(res => res.json())
};

export async function sendLike (data : any) {
    return await fetch(`http://localhost:4000/post/like`, {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : `${localStorage.getItem('token')}`
        },
    }).then(res => res.json())
};

export async function sendDeleteUser (data : any) {
    return await fetch(`http://localhost:4000/user/deleteuser`, {
        method: 'DELETE',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : `${localStorage.getItem('token')}`
        },
    }).then(res => res.json())
};

export async function sendRequestFriend(data : any) {
    return await fetch(`http://localhost:4000/user/addfriendrequest`, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : `${localStorage.getItem('token')}`
        },
    }).then(res => res.json())
};

export async function sendDeleteRequestFriend(data : any) {
    return await fetch(`http://localhost:4000/user/deletefriendrequest`, {
        method: 'DELETE',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : `${localStorage.getItem('token')}`
        },
    }).then(res => res.json())
};

export async function sendRefuseFriend(data : any) {
    return await fetch(`http://localhost:4000/user/refusefriend`, {
        method: 'DELETE',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : `${localStorage.getItem('token')}`
        },
    }).then(res => res.json())
};






