import {createStore, combineReducers, applyMiddleware} from "redux";
import authReducer from '../reducers/auth.reducer';
import searchReducer from '../reducers/search.reducer';
import chatReducer from '../reducers/chat.reducer';
import thunk from "redux-thunk";

export const rootReducer = combineReducers({
    auth: authReducer,
    search: searchReducer,
    chat: chatReducer
});

export const store = createStore(rootReducer, applyMiddleware(thunk));
