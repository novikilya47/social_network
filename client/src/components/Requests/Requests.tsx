import {connect} from "react-redux";
import {useEffect} from "react";
import {getUser} from '../../thunk/auth.thunk';
import AvatarBlock from "../UserPage/AvatarBlock";
import { addFriend, refuseFriend} from "../../thunk/user.thunk";
import { useHistory } from "react-router-dom";

function Requests(props : any) {    
    useEffect(() => {props.getUser()}, [])

    const addFriend = (id:any) =>{
        let friends = {
            firstUserId: props.user._id,
            secondUserId: id
        }
        props.addFriend(friends)
    }

    const refuseFriend = (id:any) =>{   
        let friends = {
            firstUserId: props.user._id,
            secondUserId: id
        }
        props.refuseFriend(friends)
    }

    let history = useHistory();

    const redirect = (usersList: any) =>{
        let id = usersList._id;
        history.push(`/profile/targetFriend/${id}`);
    }

        return (
            <div>{props.loading ? null : <div className = "Profile">
                <div className = "Left_side">
                    <div className = "Left_side_sticky">
                        <AvatarBlock/>   
                    </div>
                </div>

                <div className = "Right_side">
                    <div className = "Name"><h1>Хотят вас добавить в друзья</h1></div>
                        {props.user.invitations.map((invitation: any) => (
                        <div><div className = "Users_list_block" key = {invitation._id}>
                            <div className = "Users_list_ava_block">
                                <img src ={invitation.avatar} alt = "картинка"></img>
                            </div>
                            <div>{invitation.name} {invitation.surname}</div> 
                            <button className = "Users_list_invite" onClick ={() => redirect(invitation)}>Перейти в профиль</button>  
                        </div> 
                        <div className = "Button_container">
                            <button className = "Users_list_invite" onClick ={() => addFriend(invitation._id)}>Принять в друзья</button>
                            <button className = "Users_list_invite" onClick ={() => refuseFriend(invitation._id)}>Отклонить предложение</button> 
                        </div></div>))}


                    <div className = "Name"><h1>Ваши запросы на дружбу</h1></div>
                    {props.user.requests.map((request: any) => (
                    <div><div className = "Users_list_block" key = {request._id}>
                        <div className = "Users_list_ava_block">
                            <img src ={request.avatar} alt = "картинка"></img>
                        </div>
                        <div>{request.name} {request.surname}</div> 
                        <button className = "Users_list_invite" onClick ={() => redirect(request)}>Перейти в профиль</button>  
                    </div></div>))}
                    
                </div>
            </div>}</div>
        )
}

const mapDispatchToProps = (dispatch : any) => ({
    getUser: () => {dispatch(getUser())},
    addFriend: (data:any) => {dispatch(addFriend(data))},
    refuseFriend: (data:any) => {dispatch(refuseFriend(data))},
});

const mapStateToProps = (state : any) => ({
    user : state.auth.currentUser,
    loading : state.auth.loading,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Requests);