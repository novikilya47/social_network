import {connect} from "react-redux";
import {useEffect} from "react";
import {getUser} from '../../thunk/auth.thunk';
import PostBlock from './PostBlock'
import AvatarBlock from "./AvatarBlock";


function UserPage(props : any) {    
    useEffect(() => {props.getUser()}, [])
        return ( 
            <div>{props.loading ? null : <div className = "Profile">
                <div className = "Left_side">
                    <div className = "Left_side_sticky">
                        <AvatarBlock/>   
                    </div>
                </div>

                <div className = "Right_side">
                    <div className = "Name"><h1>{props.user.name} {props.user.surname}</h1></div>
                    <PostBlock/>
                </div>
            </div>}</div>          
        )
}

const mapDispatchToProps = (dispatch : any) => ({
    getUser: () => {dispatch(getUser())},
});

const mapStateToProps = (state : any) => ({
    user : state.auth.currentUser,
    loading : state.auth.loading
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserPage);