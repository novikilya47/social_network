import {connect} from "react-redux";
import {useState} from "react";
import {avatar} from "../../thunk/user.thunk";
import { Link } from "react-router-dom";

function UserPage(props : any) { 
    const [img, setImg] = useState<string>("https://i.pinimg.com/originals/0c/a9/e2/0ca9e28dcb12dc698cfd2beda6d6fa64.jpg");
    let [vision, setVision] = useState(false);

    const changeAvatar = () => {
        let avatar = {
            _id: props.user._id,
            avatar: img,
        };
        props.avatar(avatar)
        setVision(vision = false)
    }

    const changeVision = () =>{
        vision === false ? setVision(vision = true) : setVision(vision = false)
    }

        return (
                <div>
                    <img className="Avatar" src = {props.user.avatar} alt = "картинка"></img>
                    <p className="Avatar_name">{props.user.name} {props.user.surname}</p>
                    <button className="Avatar_button"  onClick={changeVision}>Обновить фотографию</button>
                    <div>{vision === false ? null : 
                        <div> 
                            <div className = "Avatar_input_field"><p>Ссылка на изображение: </p><textarea value={img} onChange={(e) => setImg(e.target.value)}/></div>
                            <div className="State_buttons">
                                    <button onClick={changeAvatar}>Отправить</button>
                            </div>
                        </div>}
                    </div> 
                    <p className = "Menu_link"><Link to = "/profile"><div className="material-icons-outlined menu">person</div><div>Моя страница</div></Link></p>
                    <p className = "Menu_link"><Link to = "/profile/findFriends"><div className="material-icons-outlined menu">search</div><div>Поиск</div></Link></p>
                    <p className = "Menu_link"><Link to = "/profile/friends"><div className="material-icons-outlined menu">groups</div><div className = 'Invitatoins'><div>Друзья</div> <div>{props.user.friends.length}</div></div></Link></p>
                    <p className = "Menu_link"><Link to = "/profile/lenta"><div className="material-icons-outlined menu">view_list</div><div>Лента</div></Link></p>
                    <p className = "Menu_link"><Link to = "/profile/messages"><div className="material-icons-outlined menu">email</div><div>Сообщения</div></Link></p>
                    <p className = "Menu_link"><Link to = "/profile/requests"><div className="material-icons-outlined menu">rule</div><div className = 'Invitatoins'><div>Запросы в друзья</div> <div>{props.user.invitations.length}</div></div></Link></p>
                </div> 
        )
}

const mapDispatchToProps = (dispatch : any) => ({
    avatar: (data : any) => {dispatch(avatar(data))},
});

const mapStateToProps = (state : any) => ({
    user : state.auth.currentUser
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserPage);