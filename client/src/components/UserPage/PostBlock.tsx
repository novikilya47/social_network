import { useState } from "react";
import {connect} from "react-redux";
import {post, deletePost, postComment, deleteComment, likePost} from "../../thunk/user.thunk";
import dateFormat from 'dateformat';
import { useHistory } from "react-router-dom";

function PostBlock(props : any) {  
    const [img, setImg] = useState<string>("https://vjoy.cc/wp-content/uploads/2019/07/1-1.jpg");
    const [text, setText] = useState<string>("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
    let [vision, setVision] = useState(false);
    let [textComment, setTextComment] = useState<string>("Комментарий")
    let [visionComment, setVisionComment] = useState(false);
    let [visionId, setVisionId] = useState('');

    const changeVision = () =>{
        vision === false ? setVision(vision = true) : setVision(vision = false)
    }

    const changeVisionComment = (idPost : any) =>{
        visionComment === false ? setVisionComment(visionComment = true) : setVisionComment(visionComment = false)
        setVisionId(visionId = idPost);
    }

    const createPost = () =>{
        let time = new Date();
        let currentTime = dateFormat(time, "dd/mm/yyyy в H:MM");
        let post = {
            _id: props.user._id,
            img: img,
            text: text,
            date: currentTime
        }
        props.addPost(post);
        setVision(vision = false) 
    }

    const createPostСomment = (id : any) =>{
        let time = new Date();
        let currentTime = dateFormat(time, "dd/mm/yyyy в H:MM");
        let comment = {
            user_id: props.user._id,
            post_id: id,
            text: textComment,
            date: currentTime
        }
        props.addPostComment(comment);
        setVisionComment(visionComment = false) 
    }

    const delete_post = (postid : any) =>{
        let post = {
            userId: props.user._id,
            postId: postid
        }
        props.deletePost(post);
    }

    const delete_comment = (commentid : any, postid : any) =>{
        let comment = {
            userId: props.user._id,
            commentId: commentid,
            postId: postid,
        }
        props.deleteComment(comment);
    }

    const like = (postid : any) => {
        let likeObj = {
            userId : props.user._id,
            postId: postid
        }
        props.like(likeObj)
    }

    let history = useHistory();

      const redirect = (Id: any) =>{
          let id = Id;
          id === props.user._id ? history.push(`/profile`) : history.push(`/profile/targetFriend/${id}`)
      }
    
    return (
        <div>
            <button className="Post_button"  onClick={changeVision}>{vision === false ?"Создать пост":"Закрыть"}</button>
            <div>{vision === false ? null : 
                <div> 
                    <div className = "Text_input_field"><p>Текст поста: </p><textarea value={text} onChange={(e) => setText(e.target.value)}/></div>
                    <div className = "Image_input_field"><p>Ссылка на изображение: </p><textarea value={img} onChange={(e) => setImg(e.target.value)}/></div>
                    <div className="State_buttons">
                        <button className="Post_button_post" onClick={createPost}>Отправить</button>
                    </div>
                </div>}
            </div> 

           
            {props.user.posts.map((posts: any) => (
                <div key = {posts._id} className = "Post_block">
                    <div className = "Post_block_user_obert">
                        <div className = "Post_block_user">
                            <div className = "Post_block_user_ava">
                                <img src ={posts.user.avatar} alt ="картинка"></img>
                            </div>

                            <div>
                                <div className = "Post_block_user_name" >{posts.user.name} {posts.user.surname}</div>
                                <div className = "Post_block_user_date">{posts.date}</div>
                            </div>
                        </div>
                        <span className="material-icons-outlined" onClick ={() => delete_post(posts._id)}>close</span>
                    </div>

                    <div className = "Post_block_info">
                        {posts.text ? <p className = "Post_block_text">{posts.text}</p> : null}
                        {posts.img ? <img className = "Post_block_img" src ={posts.img} alt = "картинка"></img>  : null }
                        <span className="likes">{posts.likes.length}</span> 
                        <span className="material-icons-outlined like" onClick ={() =>like(posts._id)}>favorite</span>
                        <span className="likes">{posts.comments.length}</span> 
                        <span className="material-icons-outlined" onClick ={() => changeVisionComment(posts._id)}>chat_bubble_outline</span>
                    </div> 

                    {(visionComment !== false && visionId === posts._id)  ? 
                    <div>
                        {posts.comments.map((comment: any) => (
                                <div key = {comment._id} className = "Post_block_user_obert">
                                <div className = "Post_block_user">
                                    <div className = "Post_block_user_ava" onClick ={() => redirect(comment.user._id)}>
                                        <img src ={comment.user.avatar} alt = "картинка"></img>
                                    </div> 

                                    <div>
                                        <div>{comment.user.name} {comment.user.surname}</div>
                                        <div>{comment.text}</div>
                                    </div>
                                </div>
                                {(comment.user._id===props.user._id ) ? <span className="material-icons-outlined" onClick ={() => delete_comment(comment._id, posts._id)}>close</span> : null}
                            </div>
                            ))}
                        
                        <div> 
                            <div className = "Text_input_field"><p>Комментарий: </p><textarea value={textComment} onChange={(e) => setTextComment(e.target.value)}/></div>
                            <div className="State_buttons">
                                <button className="Post_button_post" onClick={() =>createPostСomment(posts._id)}>Отправить</button>
                            </div>
                        </div>
                    </div> : null}
                </div>         
            )).reverse()}
        </div>
    )
}

const mapDispatchToProps = (dispatch : any) => ({
    addPost: (data : any) => {dispatch(post(data))},
    deletePost: (data : any) => {dispatch(deletePost(data))},
    addPostComment: (data : any) => {dispatch(postComment(data))},
    deleteComment: (data : any) => {dispatch(deleteComment(data))},
    like:(data : any) => {dispatch(likePost(data))},
});

const mapStateToProps = (state : any) => ({
    user : state.auth.currentUser,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PostBlock);