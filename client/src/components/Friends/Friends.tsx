import {connect} from "react-redux";
import {useEffect} from "react";
import {getUser} from '../../thunk/auth.thunk';
import AvatarBlock from "../UserPage/AvatarBlock";
import {useHistory } from "react-router-dom";

function Friends(props : any) {    
    useEffect(() => {props.getUser()}, [])
    let history = useHistory();

    const redirect = (usersList: any) =>{
        let id = usersList._id;
        history.push(`/profile/targetFriend/${id}`);
    }

        return (
            <div>{props.loading ? null : <div className = "Profile">
                <div className = "Left_side">
                    <div className = "Left_side_sticky">
                        <AvatarBlock/>
                    </div>
                </div>

                <div className = "Right_side">
                    <div>
                    <div className = "Name"><h1>Друзья</h1></div>
                        {props.user.friends.map((friends : any) => (
                            <div className = "Users_list_block" key = {friends._id}>
                                <div className = "Users_list_ava_block">
                                    <img src ={friends.avatar} alt = "картинка"></img>
                                </div>
                                <div>{friends.name} {friends.surname}</div>  
                                <button className = "Users_list_invite" onClick ={() => redirect(friends)}>Перейти в профиль</button>
                            </div>  
                        ))} 
                    </div>
                </div>  
            </div>}</div> 
        )
}

const mapDispatchToProps = (dispatch : any) => ({
    getUser: () => {dispatch(getUser())},
});

const mapStateToProps = (state : any) => ({
    user : state.auth.currentUser,
    loading : state.auth.loading
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Friends);