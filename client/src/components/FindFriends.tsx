import {connect} from "react-redux";
import {useEffect, useState} from "react";
import {getUser} from '../thunk/auth.thunk';
import {getAllUsers} from '../thunk/search.thunk';
import AvatarBlock from "./UserPage/AvatarBlock";
import {useHistory} from "react-router-dom";
import { findAll } from "../services/search.service";

function FindFriends(props : any) {    
    const [userList, setUserList] = useState([])
    const [name, setName] = useState('');
    useEffect(() => {props.getUser()}, []);
    useEffect(() => {(async () => {const res = await findAll(); setUserList(res.usersList); props.getAllUsers()})()}, [])

    let tren = (e : any) => {
        setUserList(props.usersList)
        setName(e.target.value)
    }

    let history = useHistory();

    const redirect = (usersList: any) =>{
        let id = usersList._id;
        id === props.user._id ? history.push(`/profile`) : history.push(`/profile/targetFriend/${id}`);
    }

        return (
            <div>{props.loading ? null : <div className = "Profile">
                <div className = "Left_side">
                    <div className = "Left_side_sticky">
                        <AvatarBlock/>
                    </div>
                </div>

                <div className = "Right_side">
                    <div>
                        <div className = "Name"><h1>Найти пользователя</h1></div>
                        <div className="User_text">Навигация по имени</div>
                        <textarea className="User_textarea" value={name} onChange={(e)=>tren(e)}></textarea>

                        <div>Список пользователей</div>
                        {userList.
                        filter((user:any)=>(user.name.toLowerCase() + ' ' + user.surname.toLowerCase()).includes(name.toLowerCase())).
                        map((usersList: any) => (
                            <div className = "Users_list_block" key = {usersList._id}>
                                <div className = "Users_list_ava_block">
                                    <img src ={usersList.avatar} alt = "картинка"></img>
                                </div>
                                <div>{usersList.name} {usersList.surname}</div>  
                                <button className = "Users_list_invite" onClick ={() => redirect(usersList)}>Перейти в профиль</button>
                            </div>  
                        ))}
                    </div>
                </div>  
            </div>}</div> 
        )
}

const mapDispatchToProps = (dispatch : any) => ({
    getUser: () => {dispatch(getUser())},
    getAllUsers: () => {dispatch(getAllUsers())}
});

const mapStateToProps = (state : any) => ({
    user : state.auth.currentUser,
    usersList : state.search.usersList,
    loading : state.auth.loading
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FindFriends);