import {connect} from "react-redux";
import {exitUser} from "../../actionCreators/action";
import {useHistory} from "react-router-dom"

function Navbar (props : any) {

    const isAuth = props.auth.isAuth;
    let history = useHistory();

    const exit = () => {
        props.exitUser();
        history.push('/');
    }
    
        return(
            <nav className="Navbar">
                <div><p>Cоциальная сеть</p></div>
                {isAuth && <button className = "Exit_button" onClick={exit}><span className="material-icons-outlined">logout</span>Выйти</button>}
            </nav>
        )
}

const mapDispatchToProps = (dispatch : any) => ({
    exitUser: () => {dispatch(exitUser())},
});

const mapStateToProps = (state : any) => ({
    auth : state.auth
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Navbar);
