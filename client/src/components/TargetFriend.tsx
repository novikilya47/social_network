import {connect} from "react-redux";
import {useEffect, useState} from "react";
import {getUser, getFriend} from '../thunk/auth.thunk';
import {addFriend, deleteComment, deleteFriend, likePost, postComment, requestFriend, deleteRequestFriend, refuseFriend} from '../thunk/user.thunk';
import AvatarBlock from "./UserPage/AvatarBlock";
import dateFormat from 'dateformat';
import { useHistory } from "react-router-dom";

function TargetFriend(props : any) {    
    useEffect(() => {props.getUser()}, [])
    useEffect(() => {props.getFriend({_id : window.location.pathname.substr(22)})}, [])
    let [textComment, setTextComment] = useState<string>("Комментарий")
    let [visionComment, setVisionComment] = useState(false);
    let [visionId, setVisionId] = useState('');

    const changeVisionComment = (idPost : any) =>{
        visionComment === false ? setVisionComment(visionComment = true) : setVisionComment(visionComment = false)
        setVisionId(visionId = idPost);
    }

    const deleteFriend = () =>{
        let friends = {
            firstUserId: props.user._id,
            secondUserId: props.targetUser._id
        }
        props.deleteFriend(friends)
    }

    const createPostСomment = (id : any) =>{
        let time = new Date();
        let currentTime = dateFormat(time, "dd/mm/yyyy в H:MM");
        let comment = {
            user_id: props.user._id,
            post_id: id,
            text: textComment,
            date: currentTime,
            targetUser_id : props.targetUser._id
        }
        props.addPostComment(comment);
        setVisionComment(visionComment = false) 
    }

    const delete_comment = (commentid : any, postid : any) =>{
        let comment = {
            userId: props.user._id,
            commentId: commentid,
            postId: postid,
            targetUserId : props.targetUser._id
        }
        props.deleteComment(comment);
    }

    const like = (postid : any) => {
        let likeObj = {
            userId : props.user._id,
            postId: postid,
            targetUserId: props.targetUser._id
        }
        props.like(likeObj)
    }

    const requestFriend = () => {
        let request = {
            firstUserId: props.user._id,
            secondUserId: props.targetUser._id
        }
        props.requestFriend(request)
    }

    const deleteRequestFriend = () => {
        let request = {
            firstUserId: props.user._id,
            secondUserId: props.targetUser._id
        }
        props.deleteRequestFriend(request)
    }

    const addFriend = (id:any) =>{
        let friends = {
            firstUserId: props.user._id,
            secondUserId: id
        }
        props.addFriend(friends)
    }

    const refuseFriend = (id:any) =>{   
        let friends = {
            firstUserId: props.user._id,
            secondUserId: id
        }
        props.refuseFriend(friends)
    }

    const checkFriend = props.user.friends.some((element:{_id: any;}) => {
        if (props.targetUser._id !== undefined && element._id !== undefined) {return element._id === props.targetUser._id}
    })

    const checkRequest = props.user.requests.some((element: {_id:any}) => {
        if (props.targetUser._id !== undefined && element._id !== undefined) {return element._id === props.targetUser._id}   
    })

    const checkInvitation = props.user.invitations.some((element: {_id:any}) => {
        if (props.targetUser._id !== undefined && element._id !== undefined) {return element._id === props.targetUser._id} 
    })

    let history = useHistory();

    const redirect = (Id: any) =>{
        let id = Id;
        id === props.user._id ? history.push(`/profile`) : history.push(`/profile/targetFriend/${id}`)
    }

        return (
            <div>{props.loading ? null : <div className = "Profile">
                <div className = "Left_side">
                    <div className = "Left_side_sticky">
                        <AvatarBlock/>
                    </div>
                </div>

                <div className = "Right_side">
                    <div>
                        <img className="Target_user_avatar" src = {props.targetUser.avatar} alt = "картинка"></img>
                        <h2>{props.targetUser.name} {props.targetUser.surname}</h2>

                        {checkFriend ? <button className = "Target_user_invite" onClick={deleteFriend}>Удалить из друзей</button> : 
                        checkInvitation ? 
                        <div className = "Button_container">
                            <button className = "Users_list_invite" onClick ={() => addFriend(props.targetUser._id)}>Принять в друзья</button>
                            <button className = "Users_list_invite" onClick ={() => refuseFriend(props.targetUser._id)}>Отклонить предложение</button> 
                        </div> :
                        checkRequest ? <button className = "Target_user_invite" onClick={deleteRequestFriend}>Отменить запрос</button> : <button className = "Target_user_invite" onClick={requestFriend}>Отправить запрос</button>}
                        
                        {props.targetUser.posts.map((posts: any) => (
                            <div key = {posts._id} className = "Post">
                                <div className = "Post_block_user_obert">
                                    <div className = "Post_block_user">
                                        <div className = "Post_block_user_ava">
                                            <img src ={posts.user.avatar} alt = "картинка"></img>
                                        </div>

                                        <div>
                                            <div className = "Post_block_user_name" >{posts.user.name} {posts.user.surname}</div>
                                            <div className = "Post_block_user_date">{posts.date}</div>
                                        </div>
                                    </div>
                                </div>

                                <div className = "Post_block_info">
                                    {posts.text ? <p className = "Post_block_text">{posts.text}</p> : null}
                                    {posts.img ? <img className = "Post_block_img" src ={posts.img} alt = "картинка"></img>  : null }
                                    <span className="likes">{posts.likes.length}</span> 
                                    <span className="material-icons-outlined like" onClick ={() =>like(posts._id)}>favorite</span>
                                    <span className="likes">{posts.comments.length}</span> 
                                    <span className="material-icons-outlined" onClick ={() => changeVisionComment(posts._id)}>chat_bubble_outline</span>
                                </div>

                                {(visionComment !== false && visionId === posts._id)  ? 
                                <div>
                                    {posts.comments.map((comment: any) => (
                                            <div key = {comment._id} className = "Post_block_user_obert">
                                            <div className = "Post_block_user">
                                                <div className = "Post_block_user_ava" onClick ={() => redirect(comment.user._id)}>
                                                    <img src ={comment.user.avatar} alt = "картинка"></img>
                                                </div> 

                                                <div>
                                                    <div>{comment.user.name} {comment.user.surname}</div>
                                                    <div>{comment.text}</div>
                                                </div>
                                            </div>
                                            {(comment.user._id===props.user._id ) ? <span className="material-icons-outlined" onClick ={() => delete_comment(comment._id, posts._id)}>close</span> : null}
                                        </div>
                                        ))}
                                    
                                    <div> 
                                        <div className = "Text_input_field"><p>Комментарий: </p><textarea value={textComment} onChange={(e) => setTextComment(e.target.value)}/></div>
                                        <div className="State_buttons">
                                            <button className="Post_button_post" onClick={() =>createPostСomment(posts._id)}>Отправить</button>
                                        </div>
                                    </div>
                                </div> : null}       
                            </div>  
                        ))}   
                    </div>
                </div>  
            </div>}</div> 
        )
}

const mapDispatchToProps = (dispatch : any) => ({
    getUser: () => {dispatch(getUser())},
    getFriend: (data:any) => {dispatch(getFriend(data))},
    deleteFriend: (data:any) => {dispatch(deleteFriend(data))},
    addPostComment: (data:any) => {dispatch(postComment(data))},
    deleteComment: (data:any) => {dispatch(deleteComment(data))},
    like:(data:any) => {dispatch(likePost(data))},
    requestFriend: (data:any) => {dispatch(requestFriend(data))},
    deleteRequestFriend: (data:any) => {dispatch(deleteRequestFriend(data))},
    addFriend: (data:any) => {dispatch(addFriend(data))},
    refuseFriend: (data:any) => {dispatch(refuseFriend(data))},
});

const mapStateToProps = (state : any) => ({
    user : state.auth.currentUser,
    targetUser : state.search.targetUser,
    loading : state.auth.loading
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TargetFriend);