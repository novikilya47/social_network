import React, {useState} from 'react';
import {connect} from "react-redux";
import {loginUser} from "../../thunk/auth.thunk";
import {Link, useHistory} from "react-router-dom"


function AuthUser(props : any) {
    const [email, setEmail] = useState("dima@maill.ru");
    const [password, setPassword] = useState("12345");

    let history = useHistory();

    const logUser = () => {
        let user = {
            email: email,
            password: password
        };
        props.loginUser(user);  
    }

    if (props.auth.isAuth){
        history.push('/profile')
    }
    
        return (
            <div className = "Registration">
                    <div className = "Registration_text_main">Вход</div>
                    <div className = "Registration_text_discription">Переход на личную страницу</div>
                    <div className = "Registration_field"><p>Электронная почта: </p><input value={email} onChange={(e) => setEmail(e.target.value)}/></div>
                    <div className = "Registration_field"><p>Пароль: </p><input value={password} onChange={(e) => setPassword(e.target.value)}/></div>
                    
                    <div className = "Registration_obert"> 
                        <div><Link to = "/regist">Создать аккаунт</Link></div>
                        <div className="Registration_button">
                            <button onClick={logUser}>Войти</button>
                        </div>
                    </div>
                    {props.auth.loading ? <div>Loading</div> :<div>{props.auth.message}</div>}
            </div>
        )
}

const mapStateToProps = (state : any) => ({
    auth : state.auth
});

const mapDispatchToProps = (dispatch : any) => ({
    loginUser: (data : any) => {dispatch(loginUser(data))},
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AuthUser);
