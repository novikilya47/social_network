import React, {useState} from 'react';
import {connect} from "react-redux";
import { Link } from 'react-router-dom';
import {registrationNewUser} from "../../thunk/auth.thunk";

function RegistUser(props : any) {
    const [email, setEmail] = useState("dima@maill.ru");
    const [password, setPassword] = useState("12345");
    const [name, setName] = useState("Дима");
    const [surname, setSurname] = useState("Ситник");

    const regNewUser = () => {
        let user = {
            email: email,
            password: password,
            name: name,
            surname: surname
        };
        props.registrationNewUser(user)
    }

        return (
            <div className = "Registration">
                    <div className = "Registration_text_main">Регистрация</div>
                    <div className = "Registration_field"><p>Почта пользователя: </p><input value={email} onChange={(e) => setEmail(e.target.value)}/></div>
                    <div className = "Registration_field"><p>Пароль пользователя: </p><input value={password} onChange={(e) => setPassword(e.target.value)}/></div>
                    <div className = "Registration_field"><p>Имя пользователя: </p><input value={name} onChange={(e) => setName(e.target.value)}/></div>
                    <div className = "Registration_field"><p>Фамилия пользователя: </p><input value={surname} onChange={(e) => setSurname(e.target.value)}/></div>
                    
                    <div className = "Registration_obert"> 
                        <div><Link to = "/">Вход</Link></div> 
                        <div className="Registration_button">
                            <button onClick={regNewUser}>Отправить</button>
                        </div> 
                    </div>  
                    <div>{props.auth.regMessage}</div>               
            </div>
        )
}

const mapStateToProps = (state : any) => ({
    auth : state.auth
});

const mapDispatchToProps = (dispatch : any) => ({
    registrationNewUser: (data : any) => {dispatch(registrationNewUser(data))},
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegistUser);
