import {connect} from "react-redux";
import {useEffect, useState} from "react";
import {getUser} from '../../thunk/auth.thunk';
import {deleteComment, deletePost, likePost, postComment} from "../../thunk/user.thunk";
import AvatarBlock from "../UserPage/AvatarBlock";
import dateFormat from 'dateformat';
import { Link, useHistory } from "react-router-dom";

function Lenta(props : any) {    
    useEffect(() => {props.getUser()}, [])
    const [textComment, setTextComment] = useState<string>("Комментарий")
    let [visionComment, setVisionComment] = useState(false);
    let [visionId, setVisionId] = useState('');

    const changeVisionComment = (idPost : any) =>{
        visionComment === false ? setVisionComment(visionComment = true) : setVisionComment(visionComment = false)
        setVisionId(visionId = idPost);
    }

    const delete_post = (postid : any) =>{
        let post = {
            userId: props.user._id,
            postId: postid
        }
        props.deletePost(post);
    }

    const createPostСomment = (id : any) =>{
        let time = new Date();
        let currentTime = dateFormat(time, "dd/mm/yyyy в H:MM");
        let comment = {
            user_id: props.user._id,
            post_id: id,
            text: textComment,
            date: currentTime
        }
        props.addPostComment(comment);
        setVisionComment(visionComment = false) 
    }

    const delete_comment = (commentid : any, postid : any) =>{
        let comment = {
            userId: props.user._id,
            commentId: commentid,
            postId: postid,
        }
        props.deleteComment(comment);
    }

    const like = (postid : any) => {
        let likeObj = {
            userId : props.user._id,
            postId: postid
        }
        props.like(likeObj)
    }

    let friendsNews = props.user.friends.map((posts: any) => (posts.posts));
    let userNews = props.user.posts;
    let posts = userNews.concat(friendsNews).flat();

    let mapped = posts.map(function(el:any, i:any) {
        if (el !== undefined){return { index: i, value: el.date};} 
    });

    mapped.sort(function(a:any, b:any) {
        if (a.value > b.value) {
          return 1; }
        if (a.value < b.value) {
          return -1; }
        return 0;
      });

      let sortedPosts = mapped.map(function(el:any) {
        return posts[el.index];
      }).reverse();


      let history = useHistory();

      const redirect = (Id: any) =>{
          let id = Id;
          id === props.user._id ? history.push(`/profile`) : history.push(`/profile/targetFriend/${id}`)
      }

        return (
            <div>{props.loading ? null : <div className = "Profile">
                <div className = "Left_side">
                    <div className = "Left_side_sticky">
                        <AvatarBlock/>
                    </div>
                </div>

                <div className = "Right_side">
                    <div className = "Name"><h1>Лента</h1></div>
                    
                    {sortedPosts.map((posts: any) => (
                        <div key = {posts._id} className = "Post_block">
                            <div className = "Post_block_user_obert">
                                <div className = "Post_block_user">
                                    <div className = "Post_block_user_ava" onClick ={() => redirect(posts.user._id)}>
                                            <img src ={posts.user.avatar} alt = "картинка"></img>
                                    </div>
                                    <div>
                                        <div className = "Post_block_user_name" >{posts.user.name} {posts.user.surname}</div>
                                        <div className = "Post_block_user_date">{posts.date}</div>
                                    </div>
                                </div>
                                {(posts.user._id===props.user._id) ? <span className="material-icons-outlined" onClick ={() => delete_post(posts._id)}>close</span> : null}
                            </div>

                            <div className = "Post_block_info">
                                {posts.text ? <p className = "Post_block_text">{posts.text}</p> : null}
                                {posts.img ? <img className = "Post_block_img" src ={posts.img} alt = "картинка"></img>  : null } 
                                <span className="likes">{posts.likes.length}</span> 
                                <span className="material-icons-outlined like" onClick ={() =>like(posts._id)}>favorite</span>
                                <span className="likes">{posts.comments.length}</span> 
                                <span className="material-icons-outlined" onClick ={() => changeVisionComment(posts._id)}>chat_bubble_outline</span>
                            </div> 

                            {(visionComment !== false && visionId === posts._id)  ? 
                            <div>
                                {posts.comments.map((comment: any) => (
                                        <div key = {comment._id} className = "Post_block_user_obert">
                                        <div className = "Post_block_user">
                                            <div className = "Post_block_user_ava" onClick ={() => redirect(comment.user._id)} >
                                                <img src ={comment.user.avatar} alt = "картинка"></img>
                                            </div> 

                                            <div>
                                                <div>{comment.user.name} {comment.user.surname}</div>
                                                <div>{comment.text}</div>
                                            </div>
                                        </div>
                                        {(comment.user._id===props.user._id ) ? <span className="material-icons-outlined" onClick ={() => delete_comment(comment._id, posts._id)}>close</span> : null}
                                    </div>
                                    ))}
                                
                                <div> 
                                    <div className = "Text_input_field"><p>Комментарий: </p><textarea value={textComment} onChange={(e) => setTextComment(e.target.value)}/></div>
                                    <div className="State_buttons">
                                        <button className="Post_button_post" onClick={() =>createPostСomment(posts._id)}>Отправить</button>
                                    </div>
                                </div>
                            </div> : null}       
                        </div>        
                    ))}
                </div>
            </div>}</div> 
        )
}

const mapDispatchToProps = (dispatch : any) => ({
    getUser: () => {dispatch(getUser())},
    deletePost: (data : any) => {dispatch(deletePost(data))},
    addPostComment: (data : any) => {dispatch(postComment(data))},
    deleteComment: (data : any) => {dispatch(deleteComment(data))},
    like:(data : any) => {dispatch(likePost(data))},
});

const mapStateToProps = (state : any) => ({
    user : state.auth.currentUser,
    loading : state.auth.loading
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Lenta);