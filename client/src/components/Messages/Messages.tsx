import {connect} from "react-redux";
import {useEffect, useState} from "react";
import {getUser} from '../../thunk/auth.thunk';
import AvatarBlock from "../UserPage/AvatarBlock";

import { useHistory } from "react-router";

function Messages(props : any) {  
   
useEffect(() => {props.getUser()}, [])

let history = useHistory();

const redirect = (chatId: any) =>{
    let id = chatId;
    history.push(`/profile/chat/${id}`);
}

        return (
            <div>{props.loading ? null : <div className = "Profile">
                <div className = "Left_side">
                    <div className = "Left_side_sticky">
                        <AvatarBlock/>   
                    </div>
                </div>

                <div className = "Right_side">
                    <div className = "Name"><h1>Чаты</h1></div>
                    {props.user.chats.map((chat: any) => (
                        <div className = "Users_list_block" key = {chat.users.filter((user:any)=>(user._id !== props.user._id))[0]._id}>
                                <div className = "Users_list_ava_block">
                                    <img src ={chat.users.filter((user:any)=>(user._id !== props.user._id))[0].avatar} alt = "картинка"></img>
                                </div>
                                <div>{chat.users.filter((user:any)=>(user._id !== props.user._id))[0].name} {chat.users.filter((user:any)=>(user._id !== props.user._id))[0].surname}</div>  
                                <button className = "Users_list_invite" onClick ={() => redirect(chat._id)}>Перейти в чат</button>
                        </div> 
                    ))}
                </div>
            </div>}</div>
        )
}

const mapDispatchToProps = (dispatch : any) => ({
    getUser: () => {dispatch(getUser())},
});

const mapStateToProps = (state : any) => ({
    user : state.auth.currentUser,
    loading : state.auth.loading
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Messages);