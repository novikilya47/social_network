import {connect} from "react-redux";
import {useEffect,useRef,useState} from "react";
import {getUser, getChat} from '../../thunk/auth.thunk';
import AvatarBlock from "./../UserPage/AvatarBlock";
import { io } from "socket.io-client";
import { find } from "../../services/auth.service";
import { getCurrentChatSocket } from "../../actionCreators/action";
import { useHistory } from "react-router-dom";

function Chat(props : any) {   
    
    const flag = useRef<any>();
    let scroll = function scroll (){
        let b = flag.current;
        if (b !==null) {b.scrollTop = b.scrollHeight - b.clientHeight;}
    }

    const socket = io('http://localhost:4000', { transports: ['websocket', 'polling', 'flashsocket'] });
    const myId = props.user._id
    const chatId = window.location.pathname.substr(14);
    const [message, setMessage] = useState("")
    useEffect(() => {props.getUser()}, [])
    useEffect(() => {(async () => {const res = await find(); props.getChat({_id : chatId, userId : res.user._id})})()}, [])
    useEffect(() => {scroll()}, [flag.current])

    useEffect(() => {
    socket.on('tweet', (messages: any) => {
        props.getCurrentChatSocket(messages)
        scroll()
    })}, [])
    
    const sendMessage = async (mes: any) => {socket.emit("sendMessage", { mes, myId, chatId })} 
    
    const delMessage = async (mesId:any) => {socket.emit("delMessage", { myId, mesId, chatId});}

    let history = useHistory();

    const redirect = (Id: any) =>{
        let id = Id;
        id === props.user._id ? history.push(`/profile`) : history.push(`/profile/targetFriend/${id}`)
    }
    
        return (
            <div>{props.loading ? null : <div className = "Profile">
                <div className = "Left_side">
                    <div className = "Left_side_sticky">
                        <AvatarBlock/>
                    </div>
                </div>

                <div className = "Right_side">
                    <div>
                        <div className = "Name"><h1>Переписка с {props.friend.name} {props.friend.surname}</h1></div>
                        <div className= "Chat_box">
                            <div className= "Chat_box_messages" ref={flag}>
                            {props.chat.messages ? props.chat.messages.map((mess: any) => (
                                mess.author._id === myId ? 
                                    <div key = {mess._id} className = "Message_block_my">
                                        <div className = "Message_info">
                                            <p className = "Message_text">{mess.text}</p> 
                                        </div> 
                
                                        <div className = "Message_block_my_ava" onClick ={() => redirect(mess.author._id)}>
                                            <img src ={mess.author.avatar} alt = "картинка"></img>
                                        </div> 

                                        <span className="material-icons-outlined" onClick ={() => delMessage(mess._id)}>close</span> 
                                    </div>
                                :
                                    <div key = {mess._id} className = "Message_block_friend">
                                        <div className = "Message_block_friend_ava" onClick ={() => redirect(mess.author._id)}>
                                            <img src ={mess.author.avatar} alt = "картинка"></img>
                                        </div>
                                                
                                        <div className = "Message_info">
                                            <p className = "Message_text">{mess.text}</p> 
                                        </div> 
                                    </div>
                                )) 
                            : null}
                            </div>  


                            <div className ="message-box">
                                <div className = "Image_input_field"><p>Сообщение:</p><textarea className = "message-input" placeholder="Type message..." value={message} onChange={(e) => setMessage(e.target.value)}/></div>
                                <div className="State_buttons">
                                    <button className="message-submit" onClick={() => {sendMessage(message); setMessage("")}}>Отправить</button>
                                </div>
                            </div>  
                        </div>   
                    </div>
                </div>  
            </div>}</div> 
        )
}

const mapDispatchToProps = (dispatch : any) => ({
    getUser: () => {dispatch(getUser())},
    getChat: (data:any) => {dispatch(getChat(data))},
    getCurrentChatSocket: (data:any) => {dispatch(getCurrentChatSocket(data))}
});

const mapStateToProps = (state : any) => ({
    user : state.auth.currentUser,
    loading : state.auth.loading,
    chat: state.chat.targetChat,
    friend: state.chat.friend
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Chat);