import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';
 
const userSchema = new mongoose.Schema({
    name: {type: String, required: true},
    surname: {type: String, required: true}, 
    avatar : {type: String, default: "https://vk-wiki.ru/wp-content/uploads/2019/06/user-1.png"},
    password: {type: String, required: true},
    email: {type: String, unique : true, required: true},
    posts: [{type: mongoose.Schema.Types.ObjectId, ref: "post", autopopulate: {maxDepth : 3}}],
    friends: [{type: mongoose.Schema.Types.ObjectId, ref: "user", autopopulate: {maxDepth : 3}}],
    invitations: [{type: mongoose.Schema.Types.ObjectId, ref: "user", autopopulate: {maxDepth : 2}}],
    requests: [{type: mongoose.Schema.Types.ObjectId, ref: "user", autopopulate: {maxDepth : 2}}],
    chats: [{type: mongoose.Schema.Types.ObjectId, ref: "chat", autopopulate: {maxDepth : 2}}],
}, { versionKey: false });

userSchema.plugin(autopopulate);
const User = mongoose.model("user", userSchema); 

export { User, userSchema };