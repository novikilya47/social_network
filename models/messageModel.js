import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';


const messageSchema = new mongoose.Schema({
    author: {type: mongoose.Schema.Types.ObjectId, ref: "user", autopopulate: {maxDepth : 1}},
    text: {type: String},
}, { versionKey: false });

messageSchema.plugin(autopopulate);
const Message= mongoose.model("message", messageSchema); 

export { Message, messageSchema};