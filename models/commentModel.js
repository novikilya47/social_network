import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';

 
const commentSchema = new mongoose.Schema({
    user: {type: mongoose.Schema.Types.ObjectId, ref: "user", autopopulate: {maxDepth : 2}},
    text: {type: String},
    date: {type: String},
}, { versionKey: false });

commentSchema.plugin(autopopulate);
const Comment = mongoose.model("comment", commentSchema); 

export { Comment, commentSchema };