import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';

 
const postSchema = new mongoose.Schema({
    user: {type: mongoose.Schema.Types.ObjectId, ref: "user", autopopulate: {maxDepth : 4} },
    text: {type: String},
    img: {type: String},
    date: { type: String },
    comments: [{type: mongoose.Schema.Types.ObjectId, ref: "comment", autopopulate: {maxDepth : 4 },}],
    likes: {type: Array},
}, { versionKey: false });


postSchema.plugin(autopopulate);
const Post = mongoose.model("post", postSchema); 

export { Post, postSchema };