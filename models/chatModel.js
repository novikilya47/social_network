import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';

const chatSchema = new mongoose.Schema({
    users: [{type: mongoose.Schema.Types.ObjectId, ref: "user", autopopulate: {maxDepth : 2}}],
    messages: [{type: mongoose.Schema.Types.ObjectId, ref: "message", autopopulate: {maxDepth : 2}}],
}, { versionKey: false });

chatSchema.plugin(autopopulate);
const Chat = mongoose.model("chat", chatSchema); 

export {Chat, chatSchema};